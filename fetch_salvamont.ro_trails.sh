#!/usr/bin/env bash

# Sitemap
#Muntii http://m.0salvamont.org/munte/10
    #Trasee http://m.0salvamont.org/trasee/all/all && http://m.0salvamont.org/trasee/10/all
    #Cabane http://m.0salvamont.org/cabane/all/all && http://m.0salvamont.org/cabane/10/all
    #Partii http://m.0salvamont.org/partii/all/all && http://m.0salvamont.org/partii_munte/10/all
    #Varfuri
    #Harta muntelui

#Informari: http://m.0salvamont.org/informari

# Sfaturi pentru turisti
# 11-02-2013 http://m.0salvamont.org/recomand/5
# 16-05-2012 http://m.0salvamont.org/recomand/4
# 05-03-2012 http://m.0salvamont.org/recomand/2
# 11-02-2013 http://m.0salvamont.org/recomand/1

#Vremea
#Buna ziua! Dispeceratul National Salvamont, 0SALVAMONT-O725826668, va recomanda sa studiati inainte de a pleca in drumetie, site-urile de meteorologie (www.meteoblue.com, www.accuweather.com, www.meteoromania.ro) prognoza acestora pe 3 zile fiind foarte buna, sau puteti studia site-ul m.0salvamont.org unde regasiti datele meteo din zona 
    
if [[ ! -s trasee_montane.html ]]; then
    curl -s 'http://m.0salvamont.org/trasee/all/all/' > trasee_montane.html
fi

# 0. Munti

IFS=$'\n' read -d '' -r -a munti < <(paste -d ";" <(xidel -q -e '//*[@id="contents-wrapper"]/form/p/select/option/text()' --data=trasee_montane.html) <(xidel -q -e '//*[@id="contents-wrapper"]/form/p/select/option/@value' --data=trasee_montane.html))

#echo "${munti[0]}"
#exit

m=0
for munte in "${munti[@]}"
do
    if [[ $m -eq 0 ]]; then
        let m++
        continue        
    fi
    IFS=';' read -ra ADDR <<< "$munte"
    nume_munte="${ADDR[0]}"
    url_munte="${ADDR[1]}"
    echo "[$m] $nume_munte ~> $url_munte"
    let m++
done

exit
# Trasee munte: http://m.0salvamont.org/trasee/24/all

# TODO
# Detalii munte: http://m.0salvamont.org/munte/10

# Trasee marcate in Muntii Fagaras
# http://m.0salvamont.org/trasee/8/all

# Un singur traseu
# http://m.0salvamont.org/traseu/461

paste -d ";" <(xidel -q -e '//*[@id="blog-wrapper"]' --data=trasee_muntii_fagaras.html | awk '{$1=$1;print}') <(xidel -q -e '//*[@id="blog-wrapper"]/div/a/img/@src' --data=trasee_muntii_fagaras.html) <(xidel -q -e '//*[@id="blog-wrapper"]/div/a/@href' --data=trasee_muntii_fagaras.html)

# curl -s http://m.0salvamont.org/images/marcaje/7.jpg | base64

# Pagination
# Page 1: http://m.0salvamont.org/trasee/8/all/ sau http://m.0salvamont.org/trasee/8/all/1
# Page 2: http://m.0salvamont.org/trasee/8/all/2

# Find how many pages we have
number_of_pages=$(xidel -q -e '//*[@id="contents-wrapper"]/div[@class="pagination"]/a' --data=trasee_muntii_fagaras.html | tail -n 2 | head -n 1 | tr -d ' ')

# Gather trasee de pe paginiile ramase
for i in {2,,$number_of_pages} do
    paste -d ";" <(xidel -q -e '//*[@id="blog-wrapper"]' --data=trasee_muntii_fagaras.html | awk '{$1=$1;print}') <(xidel -q -e '//*[@id="blog-wrapper"]/div/a/img/@src' --data=trasee_muntii_fagaras.html) <(xidel -q -e '//*[@id="blog-wrapper"]/div/a/@href' --data=trasee_muntii_fagaras.html)
done

# Varfuri [http://m.0salvamont.org/munte/62]
xidel -q -e '//*[@id="contents-wrapper"]/ul' varfuri.html

# Harta muntelui
# http://m.0salvamont.org/get_image.php?filename=Codru-Moma_big.jpg