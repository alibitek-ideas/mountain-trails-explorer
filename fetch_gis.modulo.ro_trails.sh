#!/usr/bin/env bash

OUTPUT_DIR="data/gis.modulo.ro/kmzs"

if [[ ! -d $OUTPUT_DIR  ]]; then
    mkdir -p "$OUTPUT_DIR"
fi

awk -F'\t' 'NR > 1{gsub("\"", ""); print $2 ";" $3 ";" $5; }' data/Trasee_Montane_Romania_OSM.csv | while IFS=';' read id name link; do
    if [[ -z $name ]]; then
        wget -nc -nH -c -O "$OUTPUT_DIR/$id.kmz" -e robots=off -U 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:44.0) Gecko/20100101 Firefox/44.0' "$link"
    else
        wget -nc -nH -c -O "$OUTPUT_DIR/$name.kmz" -e robots=off -U 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:44.0) Gecko/20100101 Firefox/44.0' "$link"
    fi
done

# Fix kmz filenames
find "$OUTPUT_DIR" -type f -name 'HikingLet*' | while read fname; do
    mv -f "$fname" "$OUTPUT_DIR/$(echo "$fname" | grep -Po '(?<=osm_id=)(\d+)(?=&)').kmz"
done 