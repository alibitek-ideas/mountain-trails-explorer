#!/usr/bin/env bash

wget -qN -P data/osm http://download.geofabrik.de/europe/romania-latest.osm.pbf

osmosis --read-pbf file=data/osm/romania-latest.osm.pbf --log-progress --write-pgsql database="Romania_OSM" user="postgres"