#!/usr/bin/env python3

import json
import sys
import argparse
import os

# Usage: ./geojson_to_coordinates.py data/"Poiana Brașov - Vf. Postăvaru.geojson" "data/Poiana Brașov - Vf. Postăvaru_coordinates.csv"
# Visualize: http://www.darrinward.com/lat-long/

if __name__ == '__main__':
    parser = argparse.ArgumentParser(prog='GeoJSON to WGS84 Coordinates')
    parser.add_argument("input_file", help="GeoJSON file to parse", metavar="INPUT FILE")
    parser.add_argument("output_file", help="Output file", metavar="OUTPUT FILE")
    args = parser.parse_args()
    
    input_file = args.input_file
    output_file = args.output_file
    
    if not os.path.exists(input_file):
        raise FileNotFoundError(input_file)
    
    with open(input_file) as in_file:
        with open(output_file, "w") as out_file:
            data = json.load(in_file)
            for feature in data['features']:
                geometry_type = feature['geometry']['type']
                geometry_coordinates = feature['geometry']['coordinates']
                if geometry_type == 'Point':
                    # TODO: Use a proper CSV writer
                    out_file.write(str(geometry_coordinates[1]))
                    out_file.write(",")
                    out_file.write(str(geometry_coordinates[0]))
                    out_file.write("\n")
