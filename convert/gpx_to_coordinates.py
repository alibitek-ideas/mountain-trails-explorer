#!/usr/bin/env python2

import gpxpy
import gpxpy.gpx

gpx_file = open('route.gpx', 'r')

gpx = gpxpy.parse(gpx_file)

for track in gpx.tracks:
    for segment in track.segments:
        for point in segment.points:
            print 'Point at {0},{1}'.format(point.latitude, point.longitude)

for waypoint in gpx.waypoints:
    print 'waypoint {0} -> ({1},{2})'.format(waypoint.name, waypoint.latitude, waypoint.longitude)

for route in gpx.routes:
    print 'Route:'
    sz = len(route.points)
    for index, point in enumerate(route.points):
        if index < sz - 1:
            print '{0},{1},'.format(point.latitude, point.longitude)
        else:
            print '{0},{1}'.format(point.latitude, point.longitude)
