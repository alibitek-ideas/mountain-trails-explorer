#!/usr/bin/env bash

# https://github.com/mapbox/togeojson#convert-kml-and-gpx-to-geojson

# npm install -g togeojson 

INPUT_FILE="$1"

[[ ! -f "$INPUT_FILE" ]] && echo "Please provide an input file" && exit 1

filename=$(basename "$INPUT_FILE")
dirname=$(dirname "$INPUT_FILE")
filename="${filename%.*}"
OUTPUT_FILE="$dirname/$filename.geojson"

togeojson "$INPUT_FILE" > "$OUTPUT_FILE"
echo "Successfully converted from $INPUT_FILE (KML/GPX) to $OUTPUT_FILE (GeoJSON)"