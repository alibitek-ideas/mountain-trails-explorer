select postgis_full_version();

select ST_Point(1, 2) AS MyFirstPoint;
select ST_SetSRID(ST_Point(-77.036548, 38.895108),4326); -- using WGS 84

select count(*) from public.relations where tags->'route' = 'hiking' and tags->'type' = 'route' 

\copy (select u.name as user, r.id, tags->'name' as name, 'http://www.openstreetmap.org/relation/' || r.id::text as url, 'https://gis.modulo.ro/../OsmHiking2.0/HikingLet?type=kmzs&osm_id=' || r.id::text || '&dist=1000' as kmz, tags->'symbol' as symbol, tags->'osmc:symbol' as osmc_symbol, tags->'ref' as ref, tags->'distance' as distance from public.relations r join public.users u on r.user_id = u.id where tags->'route' = 'hiking' and tags->'type' = 'route' order by tags->'name') to '/home/alpha/Trasee_Montane_Romania_OSM.csv' with (format 'csv', delimiter E'\t', header true, FORCE_QUOTE (name));
 Trasee montane Romania OpenStreetMap 

join public.relation_members m on m.relation_id = r.id 
 
select u.name as user, r.id, tags->'name' as name, tags->'symbol' as symbol, tags->'distance' as distance, tags->'ref' as ref from public.relations r join public.users u on r.user_id = u.id where tags->'route' = 'hiking' and tags->'type' = 'route' order by r.id, tags->'name'

select distinct member_role from public.relation_members order by 1

http://www.openstreetmap.org/relation/1405181#map=13/45.6457/24.6947
Relation: Arpașu de Sus - Cabana Arpaș - Cabana Turnuri - Cabana Podragu (1405181)
select * from public.relation_members where relation_id = 1405181

select ST_X(geom) as latitude, ST_Y(geom) as longitude from (select unnest(nodes) as id from public.ways where id in (select member_id from public.relation_members where relation_id = 5429227 and member_type = 'W') and tags->'highway' = 'path') nn join public.nodes n on n.id = nn.id join way_nodes wn on n.id = wn.node_id order by wn.sequence_id

select * from public.ways where id in (select member_id from public.relation_members where relation_id = 1405181 and member_type = 'W') order by version
353740092,1,128281,'2015-06-15 06:12:06',31974199,'"highway"=>"path"'
153551840,1,65814,'2012-03-06 12:29:52',10888383,'"highway"=>"path"'
153551838,1,65814,'2012-03-06 12:29:51',10888383,'"highway"=>"path", "sac_scale"=>"mountain_hiking"'
37334868,3,128281,'2013-05-23 09:37:05',16247949,'"highway"=>"track", "surface"=>"gravel", "tracktype"=>"grade1", "smoothness"=>"horrible"'
45581812,5,65814,'2012-03-06 12:29:54',10888383,'"highway"=>"path", "sac_scale"=>"mountain_hiking"'

45581812 '{581239287,917120509,581239289,581239290,917120510,581239291,917120511}'

select version, sequence_id, ST_Y(geom) as latitude, ST_X(geom) as longitude from nodes n join way_nodes wn on wn.node_id = n.id and wn.way_id = 45581812 where n.id in (select unnest(nodes) from ways where id = 45581812)

select distinct on (latitude, longitude) * from (select n.id, sequence_id, ST_Y(geom) as latitude, ST_X(geom) as longitude from (select unnest(nodes) as id from public.ways where id in (select member_id from public.relation_members where relation_id = 1405181 and member_type = 'W')) nn join public.nodes n on n.id = nn.id join way_nodes wn on n.id = wn.node_id order by n.id, wn.sequence_id) y

CAST(REGEXP_REPLACE(COALESCE(myfield,'0'), '[^0-9]+', '', 'g') AS INTEGER)

CLC land cover import for Romania
http://wiki.openstreetmap.org/wiki/Romania_CLC_Import

-- Peaks
http://wiki.openstreetmap.org/wiki/Tag:natural=peak?uselang=en-US
select coalesce(tags->'name:ro', tags->'name:en', tags->'name') as name, CAST(REGEXP_REPLACE(COALESCE(tags->'ele','0'), '[^0-9]+', '', 'g') AS INTEGER) as elevation, ST_Y(geom) as latitude, ST_X(geom) as longitude from nodes where tags->'natural' = 'peak' order by elevation desc nulls last

-- Saddles
select tags->'name' as name, CAST(REGEXP_REPLACE(COALESCE(tags->'ele','0'), '[^0-9]+', '', 'g') AS INTEGER) as elevation, ST_Y(geom) as latitude, ST_X(geom) as longitude from nodes where tags->'natural' = 'saddle' order by elevation desc nulls last

select * from nodes where tags->'natural' = 'peak'
select * from nodes where tags->'natural' = 'saddle'
select * from ways where tags->'tourism' = 'alpine_hut' 

-- Cabane
http://www.openstreetmap.org/way/217469540
select tags->'name' as name, nodes from ways where tags->'tourism' = 'alpine_hut' order by 1 nulls last

-- Rezervatii naturale
http://wiki.openstreetmap.org/wiki/Tag:leisure=nature%20reserve?uselang=en-US
select coalesce(tags->'name:ro', tags->'name:en', tags->'name') as name, nodes from ways where tags->'leisure' = 'nature_reserve' order by 1 nulls last

-- Water
select coalesce(tags->'name:ro', tags->'name:en', tags->'name') as name, nodes from ways where tags->'natural' = 'water' order by 1 nulls last

-- Rivers
Olt river http://www.openstreetmap.org/relation/1567969
Danube http://www.openstreetmap.org/relation/89652
select coalesce(tags->'name:ro', tags->'name:en', tags->'name') as name from relations where tags->'type' = 'waterway' order by 1 nulls last


-- Elevation
http://wiki.openstreetmap.org/wiki/Key:ele

-- Boundries
Romania: http://www.openstreetmap.org/relation/90689#map=7/45.545/26.213
Arges County: http://www.openstreetmap.org/relation/2261315#map=9/44.9434/25.4773
Muntii Fagaras: http://www.openstreetmap.org/relation/2201635#map=10/45.5237/24.9500
Rezervatia Naturala Lempes http://www.openstreetmap.org/way/37081859#map=15/45.7263/25.6616

-- Counties / Judete
https://ro.wikipedia.org/wiki/Jude%C8%9Bele_Rom%C3%A2niei
41 judete
select id, version, coalesce(tags->'name:ro', tags->'name:en', tags->'name') as name from relations where tags->'place' = 'county' and tags->'type' = 'boundary' and tags->'is_in:country_code' = 'RO' order by name

http://wiki.openstreetmap.org/wiki/Tag:boundary=protected%20area?uselang=en-US

-- All
select tags->'protect_class', * from relations where tags->'boundary' = 'protected_area' order by 1

select initcap(coalesce(tags->'name:en', tags->'name:ro', tags->'name')) as name, tags->'protect_class' from relations where tags->'boundary' = 'protected_area' order by 1

select initcap(coalesce(tags->'name:en', tags->'name:ro', tags->'name')) as name from relations where tags->'boundary' = 'protected_area' and tags->'protect_class' = '1' group by 1 order by 1

-- Munti
select initcap(coalesce(tags->'name:en', tags->'name:ro', tags->'name')) as name from relations where tags->'boundary' = 'protected_area' and tags->'protect_class' = '97' group by 1 order by 1

-- Find all Relations that reach a POI (e.g. Cabana Podragu)
-- Find all Relations that start from a city/village

-- Hotels
select * from ways where tags->'tourism' = 'hotel'
select * from ways where tags->'tourism' = 'guest_house'

select tags->'name' as name, tags->'addr:city' as city, tags->'addr:street' as street, tags->'addr:housenumber' as housenumber, tags->'addr:postcode' as postcode, tags->'phone' as phone, tags->'website' as website from ways where tags->'tourism' in ('hotel', 'guest_house') order by 2, 3, 4, 1

select * from ways where tags->'tourism' = 'guest_house'

select distinct tags->'tourism' from ways order by 1
'alpine_hut'
'artwork'
'attraction'
'camp_site'
'caravan_site'
'chalet'
'gallery'
'guest_house'
'hostel'
'hotel'
'information'
'monument'
'motel'
'museum'
'picnic_site'
'restaurant'
'telegondola'
'theme_park'
'viewpoint'
'wilderness_hut'
'yes'
'zoo'
''

-- Cave entrances
select id, tags->'name' as name, tags->'description' as description, 'http://www.openstreetmap.org/node/' || id::text as url, tags->'wikipedia' as wikipedia, ST_Y(geom) as latitude, ST_X(geom) as longitude from nodes where tags->'natural' = 'cave_entrance' order by 6, 7

-- Caves
select id, tags->'name' as name, tags->'description' as description, 'http://www.openstreetmap.org/node/' || id::text as url, tags->'wikipedia' as wikipedia, ST_Y(geom) as latitude, ST_X(geom) as longitude from nodes where tags->'natural' = 'cave' order by 6, 7

-- Waterfalls
select id, tags->'name' as name, tags->'description' as description, 'http://www.openstreetmap.org/node/' || id::text as url, tags->'wikipedia' as wikipedia, ST_Y(geom) as latitude, ST_X(geom) as longitude from nodes where tags->'natural' = 'waterfall' order by 6, 7

select id, tags->'natural', tags->'name' as name, tags->'description' as description, 'http://www.openstreetmap.org/node/' || id::text as url, tags->'wikipedia' as wikipedia, ST_Y(geom) as latitude, ST_X(geom) as longitude from nodes where tags->'natural' <> '' group by tags->'natural' order by tags->'natural' 

select distinct tags->'natural' from nodes order by 1

select * from nodes n join way_nodes wn on wn.node_id = n.id join ways w on w.id = wn.way_id where n.id = 299099166 order by w.version desc

select distinct skeys(tags) from public.relations where tags->'route' = 'hiking' and tags->'type' = 'route' order by 1
